default['mailhog']['version'] = '1.0.0'
default['mailhog']['install_method'] = 'binary'

default['mailhog']['binary']['url'] = nil # Set it to override automatical generation

default['mailhog']['binary']['mode'] = 0755
default['mailhog']['binary']['path'] = '/usr/local/bin/MailHog'

default['mailhog']['binary']['prefix_url'] = 'https://github.com/mailhog/MailHog/releases/download/v'

default['mailhog']['service']['owner'] = 'root'
default['mailhog']['service']['group'] = 'root'

default['mailhog']['smtp']['ip'] = '127.0.0.1'
default['mailhog']['smtp']['port'] = 1025
default['mailhog']['smtp']['outgoing'] = nil

default['mailhog']['api']['ip'] = '0.0.0.0'
default['mailhog']['api']['port'] = 1080
default['mailhog']['ui']['ip'] = '0.0.0.0'
default['mailhog']['ui']['port'] = 1080

default['mailhog']['cors-origin'] = nil
default['mailhog']['base_hostname'] = false
default['mailhog']['hostname'] = false

default['mailhog']['storage'] = 'memory'
default['mailhog']['mongodb']['ip'] = '127.0.0.1'
default['mailhog']['mongodb']['port'] = 27017
default['mailhog']['mongodb']['db'] = 'mailhog'
default['mailhog']['mongodb']['collection'] = 'messages'

default['mailhog']['jim']['enable'] = false
default['mailhog']['jim']['accept'] = 0.99
default['mailhog']['jim']['disconnect'] = 0.005
default['mailhog']['jim']['linkspeed']['affect'] = 0.1
default['mailhog']['jim']['linkspeed']['max'] = 10240
default['mailhog']['jim']['linkspeed']['min'] = 1024
default['mailhog']['jim']['reject']['auth'] = 0.05
default['mailhog']['jim']['reject']['recipient'] = 0.05
default['mailhog']['jim']['reject']['sender'] = 0.05